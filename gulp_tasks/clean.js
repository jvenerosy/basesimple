import {exec} from 'child_process'

const clean = (cb) => {
    exec(`rm -rf ./build`, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            return
        }
        cb()
    })
}

export default clean