import browserSync from 'browser-sync'
import config from '../config'

const server = function () {
    const param = require(config.dev + config.data + '/config.json')

    let isMulti = (param.lang.length > 1)

    browserSync.init({
        server: {
            baseDir: config.dist,
            directory: isMulti,
            serveStaticOptions: {
                extensions: ["html"]
            },
            routes: {
                "/public": "./public"
            }
        },
        port: 9740,
        logLevel: "info",
        port: 9740,
        ui: {
            port: 9720,
            weinre: {
                port: 9730
            }
        }
    })
}

export default server