import gulp from 'gulp'
import webpack from 'webpack-stream'
import browserSync from 'browser-sync'
import plumber from 'gulp-plumber'
import config from '../config'
import eslint from 'gulp-eslint'
import sourcemaps from 'gulp-sourcemaps'
import uglify from 'gulp-uglify'

const js = (cb) => {
    gulp.src(config.dev + config.js + '/app.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(eslint.format())
        .pipe(webpack({
            devtool: "source-map",
            output: {
                path: config.dist + config.js,
                filename: "app.js"
            },
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['@babel/preset-env'],
                            compact: false
                        }
                    }
                ]
            },
            mode : 'development'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dist + config.js))
        .pipe(browserSync.stream())
    cb()
}

const jsBuild = (cb) => {
    gulp.src(config.dev + config.js + '/app.js')
        .pipe(eslint.format())
        .pipe(webpack({
            output: {
                path: config.dist + config.js,
                filename: "app.js"
            },
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['@babel/preset-env'],
                            compact: false
                        }
                    }
                ]
            },
            mode : 'development'
        }))
        .pipe(uglify())
        .pipe(gulp.dest(config.build + config.js))
    cb()
}

export {js, jsBuild}