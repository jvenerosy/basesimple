import gulp from 'gulp'
import twig from 'gulp-twig'
import browserSync from 'browser-sync'
import plumber from 'gulp-plumber'
import jsonlint from 'gulp-jsonlint'
import htmlmin from 'gulp-htmlmin'
import config from '../config'

const html = function (cb) {
    delete require.cache[require.resolve(config.dev + config.data + '/config.json')];
    const param = require(config.dev + config.data + '/config.json')

    const isMulti = (param.lang.length > 1)

    param.lang.map(function (lang) {
        let dest = (isMulti) ? `${config.dist}/${lang}/` : `${config.dist}/`

        delete require.cache[require.resolve(`${config.dev + config.data}/${lang}.json`)];
        let data = require(`${config.dev + config.data}/${lang}.json`)

        gulp.src(config.dev + config.html + '/**/*.twig')
            .pipe(plumber())
            .pipe(twig({
                data: {
                    trad: data,
                    lang: lang
                }
            }))
            .pipe(gulp.dest(dest))
            .pipe(browserSync.stream())
        cb()
    })
}

const jsonLint = (cb) => {
    gulp.src(config.dev + config.data + '/**/*.json')
        .pipe(plumber())
        .pipe(jsonlint())
        .pipe(jsonlint.reporter())
        .pipe(browserSync.stream())
    cb()
}

const htmlBuild = (cb) => {
    const param = require(config.dev + config.data + '/config.json')

    const isMulti = (param.lang.length > 1)

    param.lang.map(function (lang) {
        let dest = (isMulti) ? `${config.build}/${lang}/` : `${config.build}/`

        let data = require(`${config.dev + config.data}/${lang}.json`)

        gulp.src(config.dev + config.html + '/**/*.twig')
            .pipe(twig({
                data: {
                    trad: data,
                    lang: lang
                }
            }))
            .pipe(htmlmin({ collapseWhitespace: true }))
            .pipe(gulp.dest(dest))
        cb()
    })
    cb()
}

export { html, jsonLint, htmlBuild }