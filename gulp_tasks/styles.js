import gulp from 'gulp'
import sass from 'gulp-sass'
import browserSync from 'browser-sync'
import plumber from 'gulp-plumber'
import sourcemaps from 'gulp-sourcemaps'
import replace from 'gulp-string-replace'
import config from '../config'

const styles = (cb) => {
    gulp.src(config.dev + config.style.dev + '/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(config.dist + config.style.dist))
        .pipe(browserSync.stream())
    cb()
}

const stylesBuild = (cb) => {
    gulp.src(config.dev + config.style.dev + '/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(replace('/public', '../public'))
        .pipe(gulp.dest(config.build + config.style.dist))
        .pipe(browserSync.stream())
    cb()
}

export { styles, stylesBuild }