import gulp from 'gulp'
import config from '../config'
import imagemin from 'gulp-imagemin'
import imageminJpegRecompress from 'imagemin-jpeg-recompress'
import imageminPngquant from 'imagemin-pngquant'
import imageminGifSicle from 'imagemin-gifsicle'

const copy = (cb) => {
    gulp.src(`${config.public}/**/*`)
        .pipe(gulp.dest(`${config.build}/public`))

    cb()
}

const copyBuild = (cb) => {
    gulp.src([`${config.public}/**/*`, `!${config.public}/img/`])
        .pipe(gulp.dest(`${config.build}/public`))

    cb()
}

const imgBuild = (cb) => {
    gulp.src(`${config.public}/img/*`)
        .pipe(imagemin([
            imageminJpegRecompress({
                loops: 4,
                min: 80,
                max: 85,
                quality: 'high'
            }),
            imageminPngquant(),
            imageminGifSicle()
        ]))
        .pipe(gulp.dest(`${config.build}/public/img`))
    cb()
}

export {copy, copyBuild, imgBuild}