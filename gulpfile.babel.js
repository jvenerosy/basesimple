import gulp from 'gulp'
import browserSync from 'browser-sync'

import server from './gulp_tasks/server'
import { html, jsonLint, htmlBuild } from './gulp_tasks/html'
import { styles, stylesBuild } from './gulp_tasks/styles'
import { js, jsBuild } from './gulp_tasks/js'
import clean from './gulp_tasks/clean'
import copy, { copyBuild, imgBuild } from './gulp_tasks/copy'

import config from './config'

// watchers
const watch = (cb) => {
  gulp.watch([`${config.dev}/views/**/*.twig`, `${config.dev + config.data}/**/*.json`], html)
  gulp.watch(`${config.dev + config.data}/**/*.json`, jsonLint)
  gulp.watch(`${config.dev + config.style.dev}/**/*.scss`, styles)
  gulp.watch(`${config.dev + config.js}/**/*.js`, js)
  gulp.watch(`${config.public}/**/*`, reloadOnChange)
  cb()
}

const reloadOnChange = (cb) => {
  gulp.src(`${config.public}/**/*`)
    .pipe(browserSync.stream())

  cb()
}

exports.test = copy

exports.compile = gulp.series(
  clean,
  htmlBuild,
  stylesBuild,
  jsBuild,
  copyBuild,
  imgBuild
)

exports.default = gulp.series(
  gulp.parallel(html, styles, js),
  watch,
  reloadOnChange,
  server,
)