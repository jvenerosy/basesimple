import path from 'path'

const conf = {
    dev : path.resolve(__dirname + '/private/'),
    dist : path.resolve(__dirname + '/dist/'),
    build : path.resolve(__dirname + '/build/'),
    public : path.resolve(__dirname + '/public/'),
    data : '/data',
    html : '/views/pages/',
    style : {
        dev : '/styles',
        dist : '/css'
    },
    js : '/js'
}

export default conf